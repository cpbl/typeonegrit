# Some references for reading about low carb, the food guide "conspiracy," and the injustices done to auto-immune (Type I) diabetics over the last century

I'm not an expert in this field, so I'm still on a multi-month learning trajectory to get to a point where I'm confident of what I do know and where I can explain where the science is still not settled.

 If you come away from sources like the first, below, to believe that there is ZERO evidence for a causal role of saturated fat in cardiovascular disease ... you clearly need to be confident in the science, since every place you look in North America we are still told the opposite.
 
 In any case, since I sometimes share my understanding about the best science on this with others, here are some resources for someone wanting to follow up or start learning about this:

This movie (free!) is a great starting point, if you don't mind the emotional manipulation that comes with documentaries:
 https://www.youtube.com/watch?v=TUADs-CK7vI "Fat Fiction"
This is about the non-diabetes big picture -- some of the 
history, food guides, etc: it'll get your blood boiling. 

And this will get your heart in your throat, because it's about the specific 
injustices being done to people with Type I diabetes (kids in particular) 
and it's insane:

https://vimeo.com/ondemand/diabetessolution "The Diabetes Solution"

That one's not free; $6 to stream. :)  See the trailer at the above link.


After those, and more focused on science: here're the videos (or audio podcasts) that we have watched sometimes, at least the recent ones with Bret Scher:

https://www.youtube.com/c/DietDoctorVideo/videos


Much more focused on Type I DM patients, there are also Bernstein's videos, 
https://www.youtube.com/channel/UCuJ11OJynsvHMsN48LG18Ag
and his book (https://www.amazon.ca/Dr-Bernsteins-Diabetes-Solution-Achieving/dp/0316182699). He's a legend.

I'll collect written material too, including recent peer-review review articles, though I guess there's lots of trails to books, sites, etc in the above.

For a big-picture direction about the science behind all this: somewhat related to the low-carb trend is intermittent fasting. From what I understand both of these practices are probably good for every single tissue and organ in the body, and protective against most diseases you can name.  Fasting has two mechanisms of interest; one is related also to sugar metabolism but the other is entirely distinct and relates to cell autophagy / apoptosis, ie recycling.

Low carb comes in a range depending on the balance between protein and fat. Bernstein diets get up to 35% of calories from protein (and the rest from fat), while in "keto" diets the emphasis is even more on fats, with cool physiological effects.

Maybe the start of some primary literature to look at?
  - [Saturated-Fats-and-Health-A-Reassessment-and-Proposal.pdf](https://barrington-leigh.net/t1dgrit_lit/Saturated-Fats-and-Health-A-Reassessment-and-Proposal.pdf)
 
 - Best article so far on DM and diet: "Carbohydrate restriction for diabetes: rediscovering centuries-old wisdom" (2021)
 https://www.jci.org/articles/view/142246

 Over 2023-4 we'll probably be assembling more of an annotated bibliography of recent literature, as well as a history of the dispicably poor but massively influential papers over the decades.

False received wisdom:

-  "High protein damages kidneys": https://www.sciencedirect.com/science/article/pii/S0022316622109454
-  "High nutritional salt causes heart disease": 
- "High dietary saturated fat causes heart disease":
- "Extremely high LDL levels in "LMHR" phenotype of keto-dieters is dangerous and should be treated with statins.": https://www.jacc.org/doi/10.1016/j.jacadv.2024.101109
 

This is obviously a path of learning we're just starting on. Also, if you come to different conclusions than we have on any of this, please let me know!
